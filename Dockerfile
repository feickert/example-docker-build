FROM atlasamglab/stats-base:root6.23.01

COPY requirements.txt /requirements.txt

RUN python -m pip install --no-cache-dir --upgrade pip setuptools wheel && \
    python -m pip install --no-cache-dir -r /requirements.txt
